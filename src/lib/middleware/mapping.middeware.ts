import {Request, Response, NextFunction} from 'express';

const processAddress = function(billingAddress: any){
    if(billingAddress.address!="") {
        if(billingAddress.unit!="") return "Unit "+ billingAddress.unit + "," + billingAddress.address;
        else return  billingAddress.address;
    } else if(billingAddress.poBox!="") return  billingAddress.poBox;
}

export const mappingData =  function(req: Request, res: Response, next: NextFunction) {
    const userEntityRequest = req.body.user;
    const data = {
        userEntity : {
        firstName: userEntityRequest.firstName, //req.body.firstName
        lastName: userEntityRequest.lastName, //req.body.lastName
        address:processAddress(userEntityRequest.billingAddress),
        contact:{
            phone: userEntityRequest.phone,
            email: userEntityRequest.email
        }
        }
    }

    //console.log(req.body);
    req.body = data;
    //console.log(req.body);
    next();
  };
