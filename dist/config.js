"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = {
    APIEndpoint: process.env.APIENPOINT || 'http://localhost:7020',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 8060
};
//# sourceMappingURL=config.js.map