"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const processAddress = function (billingAddress) {
    if (billingAddress.address != "") {
        if (billingAddress.unit != "")
            return "Unit " + billingAddress.unit + "," + billingAddress.address;
        else
            return billingAddress.address;
    }
    else if (billingAddress.poBox != "")
        return billingAddress.poBox;
};
exports.mappingData = function (req, res, next) {
    const userEntityRequest = req.body.user;
    const data = {
        userEntity: {
            firstName: userEntityRequest.firstName,
            lastName: userEntityRequest.lastName,
            address: processAddress(userEntityRequest.billingAddress),
            contact: {
                phone: userEntityRequest.phone,
                email: userEntityRequest.email
            }
        }
    };
    req.body = data;
    next();
};
//# sourceMappingURL=mapping.middeware.js.map